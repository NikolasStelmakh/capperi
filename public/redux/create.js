import { createStore as _createStore, applyMiddleware, compose } from 'redux';
import createMiddleware from './middleware/request';
import loggerMiddleware from 'redux-logger';
import transitionMiddleware from 'nethack-frontend-core/middlewares/transitionMiddleware';



export default function createStore(reduxReactRouter, getRoutes, createHistory, client, data, serverCookies, language) {
  const middleware = [createMiddleware(client), transitionMiddleware];

  if (__DEVTOOLS__ && __CLIENT__) {
    middleware.push(loggerMiddleware());
  }

  let finalCreateStore = applyMiddleware(...middleware)(_createStore);

  finalCreateStore = reduxReactRouter({getRoutes: getRoutes.bind(this, serverCookies), createHistory})(finalCreateStore);

  const reducer = require('./modules/reducer')(serverCookies, language);
  const store = finalCreateStore(reducer, data);

  if (module.hot) {
    module.hot.accept('./modules/reducer', () => {
      store.replaceReducer(require('./modules/reducer'));
    });
  }

  return store;
}
