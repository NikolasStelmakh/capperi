import {combineReducers} from 'redux';
import {routerStateReducer as router} from 'redux-router';

import {reducer as form} from 'redux-form';

import general from './general';


export default function(serverCookies, language) {
  return combineReducers({
    form: form.normalize({

    }),
    general: general(serverCookies, language),
    router
  });
}
