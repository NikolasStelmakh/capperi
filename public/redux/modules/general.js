'use strict';

import CookieDough from 'cookie-dough';

const Antipasti = [
  { name: "Straccetti",
    info: ["Bufala-mozzarellaa, keltaisia ja punaisia Piennolo-tomaatteja, Calabrian salamia, oreganoa, ekstra-neitsytoliiviöljyä, tuoretta basilikaa", "Mozzarella di bufala, pomodorino giallo e rosso del Piennolo, soppressata Calabrese, oregano, olio EVO, basilico fresco", "Buffalo mozzarella, red gold Piennolo tomatoes, Calabrian salami, oregano, extra virgin olive oil and fresh basil"],
    price: [10, 18],
    size: ["small", "large"],
    finSize: ["2.lle", "4:lle"]},
  { name: "Tagliere di Formaggi con straccetti al forno",
    info: ["Pizzasuikaleiden kanssa bufala-mozzarellaa, savustettua bufala-mozzarellaa, gorgonzolaa ja fontinaa", "Pannocchiette con mozzarella di bufala, Pagliarella: mozzarella affumicata. Gorgonzola e Fontina", "Pizza slices with buffalo mozzarella, smoked mozzarella, gorgonzola and taleggio"],
    price: [12, 20],
    size: ["small", "large"],
    finSize: ["2.lle", "4:lle"]},
  { name: "Tagliere Capperi - The Cutting Board",
    info: ["Pizzasuikaleiden kanssa savustettua bufala-mozzarellaa, gorgonzolaa ja fontinaa, Friulin ilmakuivattua kinkkua, Calabrian salamia,pekonia, ja täytettyä munakoisoa (ekstra-neitsytoliiviöljyä, tonnikalaa, kapriksia ja tomaatteja)", "Pannocchiette con Pagliarella: mozzarella affumicata, Gorgonzola e Fontina. Prosciutto friulano, soppressata calabrese, pancetta magra e involtini di melanzane (in olio EVO ripieni di tonno, capperi e pomodori)", "Chef’s Selection of pizza slices with smoked mozzarella, gorgonzola, fontina, italian dry cured ham, Calabrian salami, italian bacon, stuffed eggplant with tuna, capers, and tomatoes in extra virgin olive oil"],
    price: [24],
    size: ["large"],
    finSize: ["4:lle"]}
];

const PizzaGourmet = [
  { name: "Margherita DOP",
    info: ["San Marzano -tomaatteja DOP, mozzarellaa DOP, ekstra-neitsytoliiviöljyä DOP, tuoretta basilikaa", "Pomodoro San Marzano DOP, fior di latte DOP, olio EVO DOP, basilico fresco", "San Marzano tomatoes PDO, mozzarella PDO, extra virgin olive oil PDO and fresh basil"],
    price: "10,50"
  },
  { name: "Bufalina DOP",
    info: ["San Marzano -tomaatteja DOP, bufala-mozzarellaa DOP, ekstra-neitsytoliiviöljyä DOP, tuoretta basilikaa", "Pomodoro San Marzano DOP, mozzarella di bufala DOP, olio EVO DOP, basilico fresco", "San Marzano tomatoes PDO, buffalo mozzarella PDO, extra virgin olive oil PDO and fresh basil"],
    price: "13"
  },
  { name: "Marinara Napoletana",
    info: ["San Marzano -tomaatteja DOP, anjovista, tuoretta valkosipulia, oreganoa, tuoretta basilikaa", "Pomodoro San Marzano DOP, alice, aglio fresco, origano e basilico fresco", "San Marzano tomatoes, anchovies, fresh garlic, oregano, extra virgin olive oil and fresh basil"],
    price: "10,50"
  },
  { name: "Diavola Calabrese",
    info: ["San Marzano -tomaatteja DOP, mozzarellaa, Calabrian soppressata-salamia, N'duja tulista ja mausteista salamilevitettä, ekstra-neitsytoliiviöljyä, tuoretta basilikaa", "Pomodoro San Marzano DOP, fior di latte, soppressata calabrese, N ́duja, basilico fresco e olio EVO", "San Marzano tomatoes, mozzarella , Calabrian dry salami, creamy hot & spicy salami, extra virgin olive oil and fresh basil"],
    price: "14"
  },
  { name: "Papaccella",
    info: ["M ozzarellaa, napolilaisia paprikoita (papaccelle), ekstra- neitsytoliiviöljyä DOP, tuoretta basilikaa", "Fior di latte, papacelle napoletane, olio EVO DOP, basilico fresco.", "Mozzarella, Neapolitan paprika, extra virgin olive oil, and fresh basil"],
    price: "14"
  },
  { name: "Capricciosa",
    info: ["San Marzano -tomaatteja DOP, mozzarellaa, artisokkaa, kinkkua, tatteja, oliiveja, ekstra-neitsytoliiviöljyä. tuoretta basilikaa", "Pomodoro San Marzano DOP, fior di latte, carciofi, prosciutto cotto naturale, funghi porcini, olive, basilico, olio EVO.", "San Marzano tomatoes, mozzarella, artichoke, natural ham, porcini mushrooms, olives, extra virgin olive oil and fresh basil"],
    price: "16"
  },
  { name: "Pom D`Or",
    info: ["Vesuviuksen keltaisia tomaatteja, mozzarellaa, ekstra-neitsytoliiviöljyä tuoretta basilikaa", "Pomodorino giallo del Vesuvio, fior di latte, olio EVO, basilico fresco.", "Vesuvius golden tomato, mozzarella, extra virgin olive oil and fresh basil"],
    price: "13,50"
  },
  { name: "Cinghiolotta",
    info: ["Parsakaalia, mozzarellaa, villisian tuoremakkaraa, ekstra-neitsytoliiviöljyä", "Cimette di friarielli, fior di latte, salamella fresca di cinghiale, olio EVO", "Broccoli, mozzarella, fresh wild boar sausage and extra virgin olive oil."],
    price: "15"
  },
  { name: "Bella Donna",
    info: ["Mozzarellaa, gorgonzolaa, saksanpähkinä-patee, kylmäsavustettua naudanlihaa, parmesaanilastuja, balsamiviinietikkaa, ekstra- neitsytoliiviöljyä", "Fior di latte, gorgonzola, crema di noci, manzo affumicato, parmigiano reggiano, aceto balsamico, olio EVO", "Mozzarella, gorgonzola, walnut cream, cold smoked beef, shavings of parmigiano reggiano, balsamic vinegar and extra virgin olive oil"],
    price: "18"
  },
  { name: "Del Casaro",
    info: ["Mozzarellaa, parmesaani, gorgonzola ja fontina", "Fior di latte, parmigiano reggiano, gorgonzola e fontina", "Mozzarellaa, parmesaani, gorgonzola ja fontina"],
    price: "14.50"
  },
  { name: "Capperi!",
    info: ["Bufala-mozzarellaa, keltaisia ja punaisia Piennolo-tomaatteja, kaprista, oreganoa, ekstra-neitsytoliiviöljyä tuoretta basilikaa", "Mozzarella di Bufala, Pomodorino giallo e rosso del Piennolo, capperi, origano, basilico fresco, olio EVO", "Mozzarella, Red Gold tomatoes, capers, oregano, extra virgin olive oil and fresh basil"],
    price: "16.50"
  },
  { name: "Brontese",
    info: ["Pistaasipestoa, mozzarellaa, Bolognan mortadellaa, sitruunan kuorta, ekstra-neitsytoliiviöljyä tuoretta basilikaa", "Pesto di pistacchio, Fior di latte, Mortadella di bologna, scorza di limone, basilico fresco e olio EVO", "Pistachio pesto, mozzarella, italian sausage from Bologna, lemon zest, extra virgin olive oil, fresh basil"],
    price: "19"
  },
  { name: "Lardo e fichi",
    info: ["Basilika-laardi , viikunoita siirapissa, mozzarellaa, ekstra-neitsytoliiviöljyä, tuoretta basilikaa", "Lardo al basilico, fichi caramellati, fior di latte, basilico fresco e olio EVO", "A Premium cut of bacon seasoned with basil, caramelized figs, mozzarella, extra virgin olive oil, fresh basil"],
    price: "19"
  },
  { name: "Cascata di Prosciutto",
    info: ["B ufala-mozzarellaa,Friulin ilmakuivattua kinkkua, oliiveja, ekstra-neitsytoliiviöljyä, tuoretta basilikaa", "Spicchi di pizza bianca, mozzarella di Bufala, prosciutto crudo Friulano, olive, basilico fresco e olio EVO", "Slices of white pizza, buffalo mozzarella, Friuli style italian ham, olives, extra virgin olive oil, fresh basil"],
    price: "22"
  },
  { name: "Quattro pomodori",
    info: ["Mozzarellaa, keltaisia ja punaisia Piennolo-tomatteja,San Marzano “sua eccellenza”-tomaatteja, ekstra-neitsytoliiviöljyä, tuoretta basilikaa", "Fior di latte, pomodorino giallo e rosso del Piennolo, pomodorino verde e il pomodoro San Marzano “sua eccel- lenza”, basilico fresco e olio EVO", "Mozzarella, Gold Red tomatoes, San Marzano “sua eccellenza” tomato, extra virgin olive oil, fresh basil"],
    price: "18"
  }
];

const Calzone = [
  { name: "Severino",
    info: ["Ricotta-juustoa, bufala-mozzarellaa, punaisia Piennolo-tomaatteja,Friulin ilmakuivattua kinkkua, rucolaa, parmesaani-lastuja, ekstra-neitsytoliiviöljyä, tuoretta basilikaa", "Ricotta fresca, mozzarella di Bufala, pomodorino rosso del Piennolo, in crudo prosciutto Friulano, rucola, scaglie di parmigiano reggiano, basilico fresco e olio EVO.", "Ricotta cheese, buffalo mozzarella, Piennolo red tomato, Friuli style italian ham, rucola, shavings of parmigiano reggiano, fresh basil and extra virgin olive oil"],
    price: "22"
  }
];

const Insalate = [
  { name: "Caprese",
    info: ["Bufala-mozzarellaa, keltaisia ja punaisia Piennolo- ja Corbara-tomaatteja, ekstra-neitsytoliiviöljyä tuoretta basilikaa", "Mozzarella di Buffala, pomodorino giallo rosso del Piennolo, basilico fresco, olio EVO.", "Buffalo mozzarella, piennolo red gold tomatoes, fresh basil and extra virgin olive oil"],
    price: "12"
  },
  { name: "Casa Savoia",
    info: ["Vihreää salaattia, punaisia Corbara-tomaatteja, caiazzane oliivi, napolilaisia paprikoita (papaccelle), parmesaani-lastuja, ekstra- neitsytoliiviöljyä", "Insalata verde, Rucola, pomodoro rosso di Corbara, olive caiazzane, peperoni in olio di oliva, scaglie di parmi- giano reggiano, olio EVO", "Lettuce, rucola, Corbara red tomatoes, Caiazzane olives, paprika in olive oil, shavings of parmigiano reggiano, chopped walnuts and extra virgin olive oil"],
    price: "12"
  },
  { name: "Benvenuti al sud",
    info: ["Vihreää salaattia, punaisia Piennolo-tomaatteja,Tropean makeaa punasipulia, bufalo-mozzarellaa, kaprista, extra-neitsytoli- iviöljyä, tuoretta basilikaa", "Insalata verde, pomodorino rosso del Piennolo, cipolla di Tropea, mozzarella di buffala, capperi, basilico, olio EVO.", "Lettuce, Piennolo red tomatoes, sweet onion from Tropea, buffalo mozzarella, capers, fresh basil and extra virgin olive oil"],
    price: "12"
  }
];

const PizzaDessert = [
  { name: "Stracetti alla Nutella",
    info: ["Pizzasuikaleiden kansaa Nutellaa", "Small pieces of dessert pizza with Nutella", "Small pieces of dessert pizza with Nutella"],
    price: [11, 19],
    size: ["small", "large"],
    finSize: ["2.lle", "4:lle"]
  },
  { name: "Pizza alla Nutella",
    info: ["Suklainen pizza, täytteenä Nutellaa, hienonnettuja hasselpähkinöitä", "Pizza con impasto al cioccolato ripiena con Nutella, grana di noccioline", "Nutella stuffed pizza crust sprinkled with hazelnuts"],
    price: [12, 22],
    size: ["small", "large"],
    finSize: ["2.lle", "4:lle"]
  },
  { name: "Pizza bebè dalla Nutella con Gelato",
    info: ["Pieni suklainen pizza täytetynä Nutellalla ja tarjoiltuna vaniljagelaton kanssa", "Nutella stuffed baby chocolate pizza served with Vanilla gelato", "Nutella stuffed baby chocolate pizza served with Vanilla gelato"],
    price: [11]
  },
  { name: "Gelato bio artigianale",
    info: ["Valitse kaksi palloa seuraavista mauista: Vanilja – suklaa – mansikka - sitruunasorbet – pistaasi - saksanpähkinä", "Choose two scoops from the following flavours: Vanilla - chocolate - strawberry - lemon sorbet - pistachio – hazelnut", "Choose two scoops from the following flavours: Vanilla - chocolate - strawberry - lemon sorbet - pistachio – hazelnut"],
    price: [7.50]
  }
];

const Bibite = [
  { name: "Galvanina",
    info: ["Bio-Cola (kafeiiniton) - sitruuna - appelsiini - veriappelsiini - verigreippi - inkivääri - chinotto - persikka tee - vihreä tee - sitruuna tee", "Bio-Cola without cafein - gassosa lemon - orange - blood orange - blood grapefruit - Ginger beer - chinotto (bitter orange flavor soda ) - Peach Tea - Green Tea - Lemon Tea", "Bio-Cola without cafein - gassosa lemon - orange - blood orange - blood grapefruit - Ginger beer - chinotto (bitter orange flavor soda )- Peach Tea - Green Tea - Lemon Tea"],
    price: "3.50"
  },
  { name: "Galvanina Acqua",
    info: ["Galvanina Sparkling and Still water ", "Galvanina Sparkling and Still water ", "Galvanina Sparkling and Still water "],
    price: "3.50"
  }
];

const Birre = [
  { name: "PERONI NASTRO AZZURRO",
    size: ["33cl pullo"],
    price: ["6.80"]
  },
  { name: "ALLA SPINA - HANA",
    size: ["Piccola - Pieni  33cl", "Grande - Iso  40cl"],
    price: ["6.50", "7.80"]
  }
];

const DATA = 'DATA';
const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';
const CHANGE_MENU = 'CHANGE_MENU';
const CHANGE_MENU_VISABILITY = 'CHANGE-MENU-VISABILITY';

const initialState = {
  languageId: null,
  menuItem: null,
  antipasti: Antipasti,
  pizzaGourmet: PizzaGourmet,
  calzone: Calzone,
  insalate: Insalate,
  pizzaDessert: PizzaDessert,
  birre: Birre,
  bibite: Bibite,
  menuVisible: false
};

export default function reducer(serverCookies, language) {
  let clientCookies = {};


  let cookieOptions = {
    path: '/'
  };

  if (serverCookies) {
    clientCookies = serverCookies;
  } else {
    clientCookies = CookieDough();
  }

  initialState.languageId = parseInt(clientCookies.get('capperiLanguageId') || '0');
  initialState.menuItem = parseInt(clientCookies.get('capperiMenuItem') || '0');
  return function(state = initialState, action = {}) {
    switch (action.type) {
      case DATA:
        return {
          ...state
        };

      case CHANGE_LANGUAGE:
        clientCookies.set('capperiLanguageId', JSON.stringify(action.id), {...cookieOptions});
        return {
          ...state,
          languageId: action.id
        };

      case CHANGE_MENU:
        clientCookies.set('capperiMenuItem', JSON.stringify(action.id), {...cookieOptions});
        return {
          ...state,
          menuItem: action.id
        };

      case CHANGE_MENU_VISABILITY:
        return {
          ...state,
          menuVisible: action.bool
      }

      default:
        return state;
    }
  }
}

export function getData() {
  return {
    type: DATA
  }
}

export function changeLanguage(id) {
  return {
    type: CHANGE_LANGUAGE,
    id
  }
}

export function changeMenuItem(id) {
  return {
    type: CHANGE_MENU,
    id
  }

}

export function changeMenuVisability(bool) {
  return {
    type: CHANGE_MENU_VISABILITY,
    bool
  }
}
