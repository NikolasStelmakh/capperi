import React, {Component} from "react";
import {Link} from "react-router";
import {connect} from "react-redux";
import Masonry from "react-masonry-component";
import {Container} from "react-grid-system";
import Paper from 'material-ui/Paper';
import {Col, Row, Hidden, Visible} from "react-grid-system";

export default class Vini extends Component {

  render() {

    return (<div>
      <Container>
        <Hidden xs>
          <Paper className="menu-page-headers" zDepth={0}>Vini</Paper>
          <Masonry className="masonry">
            <Col xs={12} sm={12} md={12} lg={6}>
              <Paper className="menu-item-paper" zDepth={0}>
                <Row>
                  <Col xs={6} sm={6} md={6}>
                    <div  className="menu-item-coffee-name">VINI BIANCHI</div>
                    <div className="menu-item-info">Valkoviinit - White wines</div>
                  </Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">12cl</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">16cl</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">pullo</Col>
                </Row>
                <Row>
                  <Col xs={6} sm={6} md={6}>
                    <div className="menu-item-coffee-name">Indesio</div>
                    <div className="menu-item-info">Pinot Grigio, Sicilia</div>
                  </Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€8</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€9</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€35</Col>
                </Row>
                <Row>
                  <Col xs={6} sm={6} md={6}>
                    <div className="menu-item-coffee-name">Capoccia</div>
                    <div className="menu-item-info">Grillo Zibibbo, Sicilia</div>
                  </Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€9</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€10.50</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€42</Col>
                </Row>
                <Row>
                  <Col xs={6} sm={6} md={6}>
                    <div className="menu-item-coffee-name">Santa Tresa Tiina Ianca</div>
                    <div className="menu-item-info">Grillo -Viognier, Sicilia (Vegaaninen +Luomu, Vegan +Organic)</div>
                  </Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€10</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€11.50</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€48</Col>
                </Row>
              </Paper>
            </Col>
            <Col xs={12} sm={12} md={12} lg={6}>
              <Paper className="menu-item-paper" zDepth={0}>
                <Row>
                  <Col xs={6} sm={6} md={6}>
                    <div className="menu-item-coffee-name">VINI ROSSI</div>
                    <div className="menu-item-info">Punaviinit - Red wines</div>
                  </Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">12cl</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">16cl</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">pullo</Col>
                </Row>
                <Row>
                  <Col xs={6} sm={6} md={6}>
                    <div className="menu-item-coffee-name">Miopasso</div>
                    <div className="menu-item-info">Nero D ́Avola Sicilia</div>
                  </Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€8</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€9</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€36</Col>
                </Row>
                <Row>
                  <Col xs={6} sm={6} md={6}>
                    <div className="menu-item-coffee-name">Capoccia</div>
                    <div className="menu-item-info">Nero D ́Avola - Cabernet Sauvignon Sicilia</div>
                  </Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€10</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€11.50</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€48</Col>
                </Row>
                <Row>
                  <Col xs={6} sm={6} md={6}>
                    <div className="menu-item-coffee-name">Barbaglio</div>
                    <div className="menu-item-info">Salento, Puglia (Vegaaninen, Vegan)</div>
                  </Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€11</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€12.50</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€51</Col>
                </Row>
                <Row>
                  <Col xs={6} sm={6} md={6}>
                    <div className="menu-item-coffee-name">Gabbiano Bellezza</div>
                    <div className="menu-item-info">Chianti Classico Toscana</div>
                  </Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€12.50</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€14</Col>
                  <Col xs={2} sm={2} md={2} className="menu-item-coffee-price">€59</Col>
                </Row>
              </Paper>
            </Col>
          </Masonry>
        </Hidden>
        <Visible xs>
          <Paper className="menu-page-headers" zDepth={0}>Vini</Paper>
          <Masonry className="masonry">
            <Paper className="menu-item-paper" zDepth={0}>
              <Col xs={12}>
                <div className="menu-item-header">VINI BIANCHI</div>
                <div className="menu-item-info">
                  <div className="menu-page-price">12cl 16cl pullo</div>
                </div>
                <div className="menu-item-coffee-name">Indesio, Capoccia, Santa Tresa Tiina Ianca</div>
              </Col>
            </Paper>
            <Paper className="menu-item-paper" zDepth={0}>
              <Col xs={12}>
                <div className="menu-item-header">VINI ROSSI</div>
                <div className="menu-item-info">
                  <div className="menu-page-price">12cl 16cl pullo</div>
                </div>
                <div className="menu-item-coffee-name">Miopasso, Capoccia, Barbaglio, Gabbiano Bellezza</div>
              </Col>
            </Paper>
          </Masonry>
        </Visible>
      </Container>
    </div>);
  }

}
