import React, {Component} from 'react';
import Paper from 'material-ui/Paper';
import {Col, Row, Hidden, Visible} from "react-grid-system";
import {Container} from "react-grid-system";


export default class Introduction extends Component {

  render() {

    return (
      <div className="intro-main">
        <div className="introduction-page">
          <Container>
            <Row>
              <Col lg={6}>
                <h2><strong>Tervetuloa Napolin aitoihin makuihin</strong></h2>
                <p>
                  Aito napolilainen pizza on Capperin hienoin luomus, joka esittelee alkuperäisiä, Suomessa ainoastaan Capperista saatavia raaka-aineita. Taikinamme tehdään hienoimmasta italialaisesta, kylmäjauhetusta täysjyvävehnästä, jonka huomaa myös ravintoarvoista: Huippukorkea valkuaisaine- ja kuitupitoisuus tekevät pizzan pohjasta aidon napolilaisen pizzan arvoista.<br/><br/>
                  Herkullisen pohjan päälle laitetaan myöskin vain parasta: Tomaatteja San Marzanosta, Vesuviuksen alueelta ja Corbarasta; kolme kertaa peräkkäin maailman parhaaksi valittua Ella Mozzarellaa Campaniasta ja luonnollisesti kaikki muut tuoreet raaka-aineet.<br/><br/>
                  Napolilainen pizza vaatii myös muutakin kuin parhaat raaka-aineet ja sitä pidetäänkin vaativimpana versiona pizzasta. Annokset pitää valmistaa erittäin nopeasti, alle minuutissa 460-asteisessa erikoisuunissa. Capperin noudattama Gourmet Pizza -tyyli yhdistää ylellisiä aineita vahvaan perinteeseen, siitä myös Associazione pizza napoletana gourmet -jäsenyys ainoana Suomessa.<br/><br/>
                  Tule ja koe ainutlaatuinen pizzaelämys ja Suomen parhaat pizzat!<br/><br/>
                </p>
              </Col>
              <Col lg={6}>
                <img className="intro-first-image" src="/images/capperi-ravintolasali-2.jpg" alt="visitors"/>
              </Col>
            </Row>
            <Row>
              <Col lg={6}>
                <img className="intro-first-image" src="/images/capperi-pizza-on-table-2.jpg" alt="visitors"/>
              </Col>
              <Col lg={6}>
                <p>
                  <h2><strong>Capperin tarina</strong></h2>
                  Tarinamme alkaa ryhmästä ystäviä, jotka muuttivat kaikki Oulunkylän lähistölle perheiden kasvaessa. Maailmalta jäi eniten mieleen hyvä ruoka, jota ei liian paljoa alueelta löytynyt, joten perheet laittoivat voimansa yhteen ja pistivät pystyyn Capperin: Paikan, josta sai hyvää ruokaa, jota pystyisi syömään vaikka joka päivä.<br/><br/>
                  Eräs ryhmästä oli <strong>Miguel Angel Papaianni</strong>, Capperin Titolare, päällikkö. Italo-argentiinalainen koki tarinan ja ravintolan omakseen ja loi pizzaiolo <strong>Severino Salzanon</strong> kanssa ruokalistan, jollaista harvemmin löytää Napolin pohjoispuolelta.<br/><br/>
                  Lopputuloksena on ravintola, joka maahantuo kaikki omat tarvikkeensa - jopa oman täysin uniikin pizzalaatikon, jossa Capperin herkullinen pizza pysyy tuoreena jopa 20 minuutin kuljetuksen ajan. Mutta mitäpä sitä ei tekisi Suomen parhaan pizzan vuoksi.<br/><br/>
                </p>
              </Col>
            </Row>
          </Container>
        </div>
          <Visible lg xl>
            <div className="intro-hidden">
              <Container>
                <Row>
                  <Col lg={6}/>
                  <Col lg={5}>
                    <p className="intro-hidden-span">
                    <h2><strong>Tilaa isollekin joukolla</strong></h2>
                    Capperista löydät yli 50 asiakaspaikkaa, joten meillä onnistuu isommatkin juhlat ja tapahtumat. Koska käytössämme on aito Napolilainen pizzauuni, pizzat valmistuvat minuutissa ja saamme isonkin seurueen kylläiseksi nopeasti.<br/><br/>
                      <strong><a href="tel:+0103269444">Soita meille ja varaa seurueellesi pöytä ja suunnitellaan samalla herkullinen ateriasi.</a></strong><br/><br/>
                    </p>
                  </Col>
                </Row>
              </Container>
            </div>
          </Visible>
      </div>
    );
  }

}
