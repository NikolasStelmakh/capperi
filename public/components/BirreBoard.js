import React, {Component} from "react";
import {Link} from "react-router";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Masonry from "react-masonry-component";
import {Container} from "react-grid-system";
import Paper from 'material-ui/Paper';
import People from 'material-ui/svg-icons/social/people';
import {Col} from "react-grid-system";
import * as generalActions from "../redux/modules/general";

@connect(
  state => ({
    birre: state.general.birre,
    languageId: state.general.languageId
  }),
  dispatch => bindActionCreators({...generalActions}, dispatch)
)

export default class BirreBoard extends Component {

  render() {
    const {birre, languageId} = this.props;
    const br = <br></br>;

    return (<div>
      <Container>
        <Paper className="menu-page-headers" zDepth={0}>Birre</Paper>
        <Masonry
          className="masonry">
          {birre.map((v, i)=>
            <Col key={i} md={4} >
              <Paper className="menu-item-paper" zDepth={0}>
                <div className="menu-item-header">{v.name}</div>
                <div className="menu-item-info">
                  <div className="menu-page-price">{v.size[0] + ' '}<span className="menu-page-price-left">{v.price[0] + '€ '}</span></div>
                  {v.size.length === 2 && <div className="menu-page-price">{v.size[1] + ' '}<span className="menu-page-price-left">{v.price[1] + '€ '}</span></div>}
                </div>
              </Paper>
            </Col>
          )}
        </Masonry>
      </Container>
    </div>);
  }

}
