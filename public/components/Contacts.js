import React, {Component} from 'react';
import {Link} from "react-router";
import {Col, Row} from "react-grid-system";
import {Container} from "react-grid-system";


export default class Contacts extends Component {

  componentDidMount() {
    window.googleApiInit = () => {
      const capperi = {lat: 60.2312312, lng: 24.9681445};
      const map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: capperi
      });
      const marker = new google.maps.Marker({
        position: capperi,
        map: map,
        title: 'Capperi'
      });
    };
  }

  render() {

    return (
      <div>
        <div className="tiles"/>
        <div className="contacts-page">
          <Container>
            <Row>
              <Col lg={6}>
                <div id="map"></div>
              </Col>
              <Col lg={4} md={6} sm={7} xs={8}>
                <div>
                  <h2><strong>Capperi</strong></h2>
                  <h3><strong>Yhteystiedot</strong></h3>
                  Käyntiosoite<br/>
                  Siltavoudintie 9, Oulunkylä, 00640 Helsinki<br/>
                  <h3><strong>Aukioloajat</strong></h3>
                  <Row>
                    <Col xs={6} sm={4} md={3} lg={4}>Ma - To:</Col>
                    <Col xs={6} sm={6} md={6} lg={3}><strong>11 - 21</strong></Col>
                  </Row>
                  <Row>
                    <Col xs={6} sm={4} md={3} lg={4}>Pe - La:</Col>
                    <Col xs={6} sm={6} md={6} lg={3}><strong>11 - 23</strong></Col>
                  </Row>
                  <Row>
                    <Col xs={6} sm={4} md={3} lg={4}>Su:</Col>
                    <Col xs={6} sm={6} md={6} lg={3}><strong>12 - 20</strong></Col>
                  </Row>
                  <a href="tel:+0103269444"><strong>puh. 010 326 9444</strong></a><br/>
                  <a href="mailto:capperi@capperi.fi?Subject=Hello%20again" target="_top"><strong>capperi@capperi.fi</strong></a><br/>
                  <a href="https://www.facebook.com/capperi.finland/"><strong>Facebook - sivut</strong></a><br/>
                  <img className="intro-first-image" src="/images/gourmet-pizza-association.png" alt="visitors"/>
                </div>
              </Col>
            </Row>
          </Container>
          <div className="contacts-page-basement">Copyright 2017- Capperi Oy</div>
        </div>
      </div>
    );
  }

}

