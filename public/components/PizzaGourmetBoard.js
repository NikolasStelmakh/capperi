import React, {Component} from "react";
import {Link} from "react-router";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Masonry from "react-masonry-component";
import Paper from 'material-ui/Paper';
import {Container} from "react-grid-system";
import {Col} from "react-grid-system";
import * as generalActions from "../redux/modules/general";

@connect(
  state => ({
    pizzaGourmet: state.general.pizzaGourmet,
    languageId: state.general.languageId
  }),
  dispatch => bindActionCreators({...generalActions}, dispatch)
)

export default class PizzaGourmetBoard extends Component {
  render() {
    const {pizzaGourmet, languageId} = this.props;
    const enDescription = "All pizzas are served with extra virgin olive oil and/or fresh basil.";
    const enBonuses = <strong>All pizzas can be made gluten free for €2 extra and/or lactose free for €1 extra</strong>;
    const fiDescription = "Pizzat tarjoillaan ekstra-neitsytoliiviöljyn ja/tai tuoreen basilikan kanssa.";
    const fiBonuses = <strong>Pizzat on saatavilla lisähintaan gluteenittomana +2€ ja/tai laktoosittomana +1€</strong>;

    return (<div>
      <Container>
        <Paper className="menu-page-headers" zDepth={0}>Pizze Gourmet</Paper>
        <Paper className="menu-gourmet-special-info" zDepth={0}>
          <div className="menu-gourmet-description">{languageId === 0 && fiDescription || enDescription}</div>
          <div className="menu-gourmet-bonuses">{languageId === 0 && fiBonuses || enBonuses}</div>
        </Paper>
        <Masonry
          className="masonry">
          {pizzaGourmet.map((v, i)=>
            <Col key={i} md={4} >
              <Paper className="menu-item-paper" zDepth={0}>
                <div className="menu-item-header">{v.name}</div>
                <div className="menu-item-info">
                  <div className="menu-page-price">{v.price}€ </div>
                  {v.info[languageId]}
                </div>
              </Paper>
            </Col>
          )}
        </Masonry>
      </Container>
    </div>);
  }

}
