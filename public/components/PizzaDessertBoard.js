import React, {Component} from "react";
import {Link} from "react-router";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Masonry from "react-masonry-component";
import People from 'material-ui/svg-icons/social/people';
import Paper from 'material-ui/Paper';
import {Container} from "react-grid-system";
import {Col} from "react-grid-system";
import * as generalActions from "../redux/modules/general";

@connect(
  state => ({
    pizzaDessert: state.general.pizzaDessert,
    languageId: state.general.languageId
  }),
  dispatch => bindActionCreators({...generalActions}, dispatch)
)

export default class PizzaDessertBoard extends Component {

  render() {
    const {pizzaDessert, languageId} = this.props;
    const fourPeople = <span style={{paddingLeft: '13px'}}><People style={{paddingTop: '6px'}} color={'#454f4c'}/><People style={{paddingTop: '6px'}} color={'#454f4c'}/></span>;

    return (<div>
      <Container>
        <Paper  className="menu-page-headers" zDepth={0}>Pizze Dolce</Paper>
        <Masonry
          className="masonry">
          {pizzaDessert.map((v, i)=>
            <Col key={i} md={4} >
              <Paper className="menu-item-paper" zDepth={0}>
                <div className="menu-item-header">{v.name}</div>
                <div className="menu-item-info">
                  <div className="menu-page-price">{ v.size && <People style={{paddingTop: '6px'}} color={'#454f4c'}/>}{v.price[0] + '€'}{v.price[1] && fourPeople}{v.price[1] && v.price[1] + '€ '}</div>
                  {v.info[languageId]}
                </div>
              </Paper>
            </Col>
          )}
        </Masonry>
      </Container>
    </div>);
  }

}
