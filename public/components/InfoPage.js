import React, {Component} from 'react';
import {List} from 'material-ui/List';
import {Container} from "react-grid-system";
import {Col, Row, Visible} from "react-grid-system";

export default class InfoPage extends Component {

  render() {

    return (
      <div className="info-page">
        <List>
        <Container className="info-text">
          <h2><strong>MARGHERITA of SAVOY, Queen of Italy, Mother of Pizza</strong></h2>
          <Row className="story-rows">
            <Visible lg xl>
              <Col lg={3} xl={2}>
                <img className="story-first-image" src="/images/logo-img.jpg" alt="story"/>
              </Col>
            </Visible>
            <Col lg={8} xl={10}>
              <div className="story-first-item">
                <br/>
                <p>The story says, that in 1889 a Pizzaiolo named <strong>Raffaele Esposito</strong> create the Pizza
                  Margherita in Naples for the Queen to celebrate Italian unity by recreating the
                  Italian flag in the form of pizza: With legendary San Marzano tomatoes, the finest
                  white Mozzarella cheese and decorated with fragrant Basil leaves – all on top of a
                  tender pizza dough with a beautiful cornicione, the fluffy and plump sides, called
                  the Pizza Margherita.</p>
                <p>A great story, but not entirely true, as the pizza itself has been recorded already in
                  1796, with Mozzarella shaped like a white flower, the daisy - or as the Italians call
                  the flower, the Margherita.</p>
                <p>The Margherita is Capperi's finest creation, portraying the ingredients only
                  available in Finland through Capperi. The finest 00 flour, with its unique high
                  protein and high fiber (6.7%!) content, the Slow Presidia ingredients like tomatoes
                  from San Marzano, Corbara and Piennolo, the Ella Mozzarella from Campania and of
                  course fresh Basil.</p>
                <p>The same principles of Italian food: Simplicity, freshness and the power of each
                  ingredient adding to the whole is what Capperi stands for, the best pizzas in
                  Finland.</p>
              </div>
            </Col>
          </Row>
          <Row className="story-rows">
            <Col lg={6}>
              <h2><strong>CAPPERI family</strong></h2>
              <p>The history of Capperi begins with a group of friends who started to settle down.
              They got married, children and bought a house in the suburbs. One thing they
              greatly missed from the larger populace was high quality food. So, many families
                pooled together their resources and built a restaurant for themselves.</p>
              <p>The new owners wanted something they could eat every day, what would be fast
              and easy to produce, have enough variety for everyone and something they all
                loved – with a bit of affordable luxury to top it off.</p>
            </Col>
            <Col lg={6}>
              <img className="intro-first-image" src="/images/story.jpg" alt="story"/>
            </Col>
          </Row>
          <Row className="story-rows">
            <Col>
              <h2><strong>Why Neapolitan pizza?</strong></h2>
              <p>One of the owning families is <strong>the family of Miguel Angel Papaianni</strong> - the titolare of
              Capperi. A long history in the hospitality and services gave him a great angle to run
              the restaurant and being a first-generation Argentinian from Italian parents, he
                suggested authentic Neapolitan pizza - a first in Finland.</p>
              <p>Once decided, the families traveled the world, testing restaurants, concepts and
              different styles of Neapolitan pizza and found a single thing they truly loved, the
                new school of Gourmet Pizza.</p>
              <p>The Neapolitan pizza is traditionally known as "food for the poor", but gourmet
              pizza takes the step a bit further focusing on the extravagant both in terms of
              flavours and ingredients, but it still is traditional and respects the authenticity of
              Neapolitan pizza – Simplicity, hand formed dough and a hot oven temperature
                ensuring the pizza is fully cooked in 90 seconds or less.</p>
            </Col>
          </Row>
          <Row className="story-rows">
            <Col lg={6}>
              <img className="intro-first-image" src="/images/contacty.jpg" alt="pizza"/>
            </Col>
            <Col lg={6}>
              <h2><strong>Capperi way of food</strong></h2>
              <p>Proper Neopolitan pizza in itself is a rare find outside Italy, but to do it the Capperi
              way is extraordinary. Every single ingredient is specifically selected for its
                individual purpose. You can ask <strong>Miguel</strong> or <strong>Chef Severino</strong> about each one, and they
                will tell you why that exact item is in our food.</p>
              <p>This has lead to the point where Capperi exclusively imports most of the items that
              are not available in Finland, including the amazing pizza box, keeping the pizzas
                oven fresh for up to 20 minutes.</p>
            </Col>
          </Row>
          <Row className="story-rows">
            <div className="info-footer">
              <h2 className="info-text-headers"><strong>The end result is the best pizza in Finland, served from a small restaurant in
                Helsinki.</strong></h2><br/>
              <h2 className="info-text-headers"><strong>Welcome!</strong></h2>
            </div>
          </Row>
          </Container>
        </List>
      </div>
    );
  }

}

