import React, {Component, PropTypes} from 'react';


export default class ScrollToTop extends Component {
  state = {
    backToTop: false,
    addBottom: false
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll.bind(this));
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll.bind(this));
  }

  handleScroll(event) {
    let footerHeight = 55;
    let target = event.currentTarget;
    let scrollTop = target.scrollTop || window.pageYOffset;
    let scrollHeight = target.scrollHeight || document.body.scrollHeight;


    if (((scrollHeight - scrollTop) <= (target.innerHeight + footerHeight))) {
      this.setState({addBottom: true});
    } else {
      this.setState({addBottom: false});
    }

    if (scrollTop < 640) {
      this.setState({backToTop: false});
    } else {
      this.setState({backToTop: true});
    }
  }

  onTop() {
    scrollTo(document.body, 0);
  }

  render() {
    return (
      <div className={this.state.backToTop ? '' : 'fade'}>
        <button
          onClick={this.onTop.bind(this)}
          type="button"
          className={this.state.addBottom ? 'margin-bottom-70 scroll-to-top' : 'scroll-to-top'}>
          <i className="fa fa-chevron-up"></i>
        </button>
      </div>
    );
  }
}
