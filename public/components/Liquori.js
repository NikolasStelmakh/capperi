import React, {Component} from "react";
import {Link} from "react-router";
import {connect} from "react-redux";
import Masonry from "react-masonry-component";
import {Container} from "react-grid-system";
import Paper from 'material-ui/Paper';
import {Col} from "react-grid-system";

export default class Liquori extends Component {

  render() {
    const br = <br></br>;

    return (<div>
      <Container>
        <Paper className="menu-page-headers" zDepth={0}>Liquori - Digestivi</Paper>
        <Masonry
          className="masonry">
            <Col xs={12} sm={12} md={12} lg={12} xl={4}>
              <Paper className="menu-item-paper" zDepth={0}>
                <div className="menu-item-info">
                  <div className="menu-page-price">4cl<span className="menu-page-price-left">€7</span></div>
                </div>
                <div className="menu-item-header">Liquirizia (Lakritsi) - Limoncello - Vecchio Amaro del Capo  Finocchio - Bergamotto</div>
                <div className="menu-item-header">Grappa Riserva 18 kk - Grappizia (Grappa-Lakritsi) Sambuca</div>
              </Paper>
            </Col>
        </Masonry>
      </Container>
    </div>);
  }

}
