import React, {Component} from "react";
import {Link} from "react-router";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Masonry from "react-masonry-component";
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from "material-ui/Card";
import Paper from 'material-ui/Paper';
import {Container} from "react-grid-system";
import {Col} from "react-grid-system";
import * as generalActions from "../redux/modules/general";

@connect(
  state => ({
    calzone: state.general.calzone,
    languageId: state.general.languageId
  }),
  dispatch => bindActionCreators({...generalActions}, dispatch)
)

export default class CalzoneBoard extends Component {

  render() {
    const {calzone, languageId} = this.props;

    return (<div>
      <Container>
        <Paper className="menu-page-headers" zDepth={0}>Calzone</Paper>
        <Masonry
          className="masonry">
          {calzone.map((v, i)=>
            <Col key={i} md={4} >
              <Paper className="menu-item-paper" zDepth={0}>
                <div className="menu-item-header">{v.name}</div>
                <div className="menu-item-info">
                  <div className="menu-page-price">{v.price}€ </div>
                  {v.info[languageId]}
                </div>
              </Paper>
            </Col>
          )}
        </Masonry>
      </Container>
    </div>);
  }

}
