import React, {Component} from "react";
import {Link} from "react-router";
import {connect} from "react-redux";
import Masonry from "react-masonry-component";
import {Container} from "react-grid-system";
import Paper from 'material-ui/Paper';
import {Col, Row} from "react-grid-system";

export default class CoffeBoard extends Component {

  render() {

    return (<div>
      <Container>
        <Paper className="menu-page-headers" zDepth={0}>CAFFÈ E TÈ</Paper>
        <Masonry className="masonry">
            <Col xs={10} sm={5} md={4}>
              <Paper className="menu-item-paper" zDepth={0}>
                <Row>
                  <Col xs={6} sm={7} md={7} className="menu-item-coffee-name">Espresso</Col>
                  <Col xs={4} sm={4} md={4} className="menu-item-coffee-price">€2,20</Col>
                </Row>
                <Row>
                  <Col xs={6} sm={7} md={7} className="menu-item-coffee-name">Macchiato</Col>
                  <Col xs={4} sm={4} md={4} className="menu-item-coffee-price">€2,20</Col>
                </Row>
                <Row>
                  <Col xs={6} sm={7} md={7} className="menu-item-coffee-name">Americano</Col>
                  <Col xs={4} sm={4} md={4} className="menu-item-coffee-price">€2,20</Col>
                </Row>
              </Paper>
            </Col>
            <Col xs={10} sm={5} md={4}>
              <Paper className="menu-item-paper" zDepth={0}>
                <Row>
                  <Col xs={6} sm={7} md={7} className="menu-item-coffee-name">Capuccino</Col>
                  <Col xs={4} sm={4} md={4} className="menu-item-coffee-price">€3,50</Col>
                </Row>
                <Row>
                  <Col xs={6} sm={7} md={7} className="menu-item-coffee-name">Tee</Col>
                  <Col xs={4} sm={4} md={4} className="menu-item-coffee-price">€2,20</Col>
                </Row>
                <Row>
                  <Col xs={6} sm={7} md={7} className="menu-item-coffee-name">Caffè Corretto</Col>
                  <Col xs={4} sm={4} md={4} className="menu-item-coffee-price">€5,00</Col>
                </Row>
              </Paper>
            </Col>
        </Masonry>
      </Container>
    </div>);
  }

}
