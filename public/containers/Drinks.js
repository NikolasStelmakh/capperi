import React, {Component} from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import BibiteBoard from '../components/BibiteBoard';
import BirreBoard from '../components/BirreBoard';
import CoffeBoard from '../components/CoffeBoard';
import Liquori from '../components/Liquori';
import Vini from '../components/Vini';
import ScrollToTop from '../components/ScrollToTop';

export default class Drinks extends Component {

  render() {
    return (
      <div className="menu-page">
        <BibiteBoard/>
        <CoffeBoard/>
        <Liquori/>
        <BirreBoard/>
        <Vini/>
        <ScrollToTop/>
      </div>
    )
  }
}
