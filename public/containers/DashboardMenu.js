import React, {Component} from "react";
import {Link} from "react-router";
import {connect} from "react-redux";
import AntipastiBoard from "../components/AntipastiBoard";
import PizzaGourmetBoard from "../components/PizzaGourmetBoard";
import CalzoneBoard from "../components/CalzoneBoard";
import InsalateBoard from "../components/InsalateBoard";
import PizzaDessertBoard from "../components/PizzaDessertBoard";
import BibiteBoard from "../components/BibiteBoard";
import BirreBoard from "../components/BirreBoard";
import CoffeBoard from "../components/CoffeBoard";
import Liquori from "../components/Liquori";
import Vini from "../components/Vini";
import ScrollToTop from '../components/ScrollToTop';

export default class DashboardMenu extends Component {

  render() {

    return (
      <div className="menu-page">
        <AntipastiBoard/>
        <InsalateBoard/>
        <CalzoneBoard/>
        <PizzaGourmetBoard/>
        <PizzaDessertBoard/>
        <BibiteBoard/>
        <CoffeBoard/>
        <Liquori/>
        <BirreBoard/>
        <Vini/>
        <ScrollToTop/>
      </div>
    )
  }
}
