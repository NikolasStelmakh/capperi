import React, {Component} from "react";
import {Link} from "react-router";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import LocalPizza from 'material-ui/svg-icons/maps/local-pizza';
import RestaurantMenu from 'material-ui/svg-icons/maps/restaurant-menu';
import FavoriteBorder from 'material-ui/svg-icons/action/favorite-border';
import MenuItem from 'material-ui/MenuItem';
import Paper from 'material-ui/Paper';
import Menu from 'material-ui/Menu';
import Divider from 'material-ui/Divider';
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar';
import {Tabs, Tab} from 'material-ui/Tabs';
import DropDownMenu from 'material-ui/DropDownMenu';
import Contacts from '../components/Contacts';
import * as generalActions from '../redux/modules/general';

@connect(
  state => ({
    languageId: state.general.languageId,
    menuVisible: state.general.menuVisible
  }),
  dispatch => bindActionCreators({...generalActions}, dispatch)
)

export default class General extends Component {

  handleChangeLanguage = (event, index, value) => {
    this.props.changeLanguage(value);
  }

  handleChangeMenu =(event, value) => {
    this.props.changeMenuItem(value);
    let item;
    switch (value) {
      case 0:
        item = '';
        break;
      case 1:
        item = 'Antipasti';
        break;
      case 2:
        item = 'PizzaGourmet';
        break;
      case 3:
        item = 'Calzone';
        break;
      case 4:
        item = 'Insalate';
        break;
      case 5:
        item = 'PizzaDessert';
        break;
      case 6:
        item = 'Drinks';
        break;
      default:
        item = '';
        break;
    }
    const path = '/menu/' + item;
    const menuVisible = this.props.menuVisible;
    this.props.history.pushState(null, path);
    this.props.changeMenuVisability(!menuVisible);
  }

  handleIntroTap() {
    this.props.history.pushState(null, '/');
    this.props.changeMenuVisability(false);
  }

  handleMenuTap() {
    const menuVisible = this.props.menuVisible;
    this.props.changeMenuVisability(!menuVisible);
  }

  handleStoryTap() {
    this.props.history.pushState(null, '/story');
    this.props.changeMenuVisability(false);
  }

  render() {
    const {languageId, menuItem} = this.props;
    const {menuVisible} = this.props;

    return (
      <div className="general">
        <div className="main">
          <div className="select-language">
            <p className="center-tag"><img className="logo" src="/images/capperi-logo-k.png"/></p>
            <div className="under-logo-text">CAPPERI AVATTU VIHDOIN, TERVETULOA!</div>
          </div>
          <Toolbar className="toolbar-language">
            <ToolbarGroup>
              <DropDownMenu
                style={{backgroundColor: '#5a5f66', marginTop: '-35px', height: '60px'}}
                menuStyle={{backgroundColor: '#5a5f66'}}
                selectedMenuItemStyle={{color: 'black'}}
                menuItemStyle={{color: 'white', fontFamily: 'Rosina-UltraBold', fontSize: '15px'}}
                labelStyle={{color: 'white', fontFamily: 'Rosina-UltraBold', fontSize: '15px'}}
                value={languageId}
                onChange={this.handleChangeLanguage} >
                <MenuItem value={0} primaryText="Suomeksi" />
                <MenuItem value={1} primaryText="Italiano" />
                <MenuItem value={2} primaryText="English" />
              </DropDownMenu>
            </ToolbarGroup>
          </Toolbar>
          <div style={{height: '0', padding: '0', margin: '0'}}><a name="top"></a></div>
          <Tabs className="tabs">
            <Tab className="tab" icon={<LocalPizza/>} label="INFO" primary={true} onActive={this.handleIntroTap.bind(this)} />
            <Tab className="tab" icon={<RestaurantMenu/>} label="MENU" primary={true} onActive={this.handleMenuTap.bind(this)} />
            <Tab className="tab" icon={<FavoriteBorder/>} label="STORY" primary={true} onActive={this.handleStoryTap.bind(this)} />
          </Tabs>
          {menuVisible && <div className="drop-down-menu-wrapper">
            <Paper className="drop-down-menu">
              <Menu
                menuItemStyle={{color: 'white', fontSize: '28px', fontFamily: 'Rosina-UltraBold', margin: '0', width: '100%'}}
                onChange={this.handleChangeMenu.bind(this)}>
                  <MenuItem value={0} primaryText="All" />
                  <Divider/>
                  <MenuItem value={1} primaryText="Antipasti"/>
                  <MenuItem value={2} primaryText="Pizza Gourmet"/>
                  <MenuItem value={3} primaryText="Calzone"/>
                  <MenuItem value={4} primaryText="Insalate"/>
                  <MenuItem value={5} primaryText="Pizza Dessert"/>
                  <Divider/>
                  <MenuItem value={6} primaryText="Drinks"/>
              </Menu>
            </Paper>
          </div>}
          {this.props.children}
        </div>
        <div className="tiles-top"/>
        <Contacts/>
      </div>
    );
  }

}
