import React from 'react';
import {Route} from 'react-router';
import NoMatchPage from 'nethack-frontend-core/components/NoMatchPage';
import General from './containers/General';
import DashboardMenu from './containers/DashboardMenu';
import InfoPage from './components/InfoPage';
import Introduction from './components/Introduction';
import AntipastiBoard from './components/AntipastiBoard';
import PizzaGourmetBoard from './components/PizzaGourmetBoard';
import CalzoneBoard from './components/CalzoneBoard';
import InsalateBoard from './components/InsalateBoard';
import PizzaDessertBoard from './components/PizzaDessertBoard';
import Drinks from './containers/Drinks';

import Acl from './helpers/acl';


export default (serverCookies, store) => {
  return (
    <Route component={General} onEnter={Acl.getMenuVariables(store, serverCookies)}>
      <Route path="/" component={Introduction} />

      <Route path="/menu/" component={DashboardMenu}/>
      <Route path="/menu/Antipasti" component={AntipastiBoard}/>
      <Route path="/menu/PizzaGourmet" component={PizzaGourmetBoard}/>
      <Route path="/menu/Calzone" component={CalzoneBoard}/>
      <Route path="/menu/Insalate" component={InsalateBoard}/>
      <Route path="/menu/PizzaDessert" component={PizzaDessertBoard}/>
      <Route path="/menu/Drinks" component={Drinks}/>

      <Route path="/story" component={InfoPage}/>

      <Route path="*" component={NoMatchPage}/>
    </Route>
  );
};
