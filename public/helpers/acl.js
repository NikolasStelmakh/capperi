'use strict';

import CookieDough from 'cookie-dough';

export default class Acl {
  static getCookies(serverCookies) {
    let result = {};

    if (serverCookies) {
      result.languageId = parseInt(serverCookies.get('capperiLanguageId'));
      result.menuItem = parseInt(serverCookies.get('capperiMenuItem'));
    } else {
      let clientCookies = CookieDough();
      result.languageId = parseInt(clientCookies.get('capperiLanguageId'));
      result.menuItem = parseInt(clientCookies.get('capperiMenuItem'));
    }
    return result;
  }

  static getMenuVariables(store, serverCookies) {
    return (nextState, replaceState) => {
      let languageId;
      let menuItem;

      try {
        languageId = store.getState().general.languageId;
        menuItem = store.getState().general.menuItem;
      } catch (err) {
        menuItem = Acl.getCookies(serverCookies).menuItem;
        languageId = Acl.getCookies(serverCookies).languageId;
      }
    }
  }
}
