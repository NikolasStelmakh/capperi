import 'babel-core/polyfill';
import 'Base64';
import 'sass/_base.scss';
import 'react-widgets/dist/css/react-widgets.css';


import 'codemirror/mode/javascript/javascript'
import 'codemirror/mode/css/css'
import 'codemirror/mode/htmlmixed/htmlmixed'
import 'codemirror/lib/codemirror.css'

require('offline-plugin/runtime').install();

import React from 'react';
import { render } from 'react-dom';
import createStore from './redux/create';
import { Provider } from 'react-redux';
import { reduxReactRouter, ReduxRouter } from 'redux-router';

import getRoutes from './routes';
import Api from 'nethack-frontend-core/helpers/api';
import createHistory from 'history/lib/createBrowserHistory';

import _ from 'lodash';
import moment from 'moment';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

const client = new Api();
const store = createStore(reduxReactRouter, getRoutes, createHistory, client, window.__data);
const dest = document.getElementById('root');


window.React = React;
window._ = _;
window.moment = moment;

const component = (
  <MuiThemeProvider>
  <ReduxRouter routes={getRoutes(undefined, store)} />
    </MuiThemeProvider>
);

render(
  <Provider store={store} key="provider">
    {component}
  </Provider>,
  dest
);
