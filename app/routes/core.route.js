'use strict';

module.exports = (app) => {
  const core = require('../controllers/core.controller');

  // Return a 404 for all undefined api, module or lib routes
  app.route('/build/*').get(core.renderNotFound);

  // Define application route
  app.route('/*').get(core.renderIndex);
};
