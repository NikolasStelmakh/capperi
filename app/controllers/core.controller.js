'use strict';

import {config} from 'nethack-frontend-core';
import React from 'react';
import ReactDOM from 'react-dom/server';
import {ReduxRouter} from 'redux-router';
import createHistory from 'history/lib/createMemoryHistory';
import {reduxReactRouter, match} from 'redux-router/server';
import {Provider} from 'react-redux';
import qs from 'query-string';
import getRoutes from '../../public/routes';
import getStatusFromRoutes from 'nethack-frontend-core/helpers/getStatusFromRoutes';
import createStore from '../../public/redux/create';
import ApiClient from 'nethack-frontend-core/helpers/api';
import Html from '../views/index.view';
import CookieDough from 'cookie-dough';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

/**
 * Render the main application page
 */
exports.renderIndex = (req, res, next) => {
  const client = new ApiClient();

  const serverCookies = new CookieDough(req);

  let language = '';

  try {
    language = req.headers['accept-language'].split(';')[0].split(',')[0].split('-')[0].toLowerCase();
  } catch (e) {
    language = 'en';
  }

  const store = createStore(reduxReactRouter, getRoutes, createHistory, client, undefined, serverCookies, language);

  function hydrateOnClient() {
    res.send('<!doctype html>\n' +
      ReactDOM.renderToString(<Html config={{
        ...config.app, jsFile, cssFile
      }} store={store}/>));
  }

  store.dispatch(match(req.originalUrl, (error, redirectLocation, routerState) => {
    if (redirectLocation) {
      res.redirect(redirectLocation.pathname + redirectLocation.search);
    } else if (error) {
      console.error('ROUTER ERROR:', error.message);
      res.status(500);
      hydrateOnClient();
    } else if (!routerState) {
      res.status(500);
      hydrateOnClient();
    } else {
      if (routerState.location.search && !routerState.location.query) {
        routerState.location.query = qs.parse(routerState.location.search);
      }

      store.getState().router.then(() => {
        const component = (
          <Provider store={store} key="provider">
            <MuiThemeProvider>
            <ReduxRouter/>
              </MuiThemeProvider>
          </Provider>
        );

        const status = getStatusFromRoutes(routerState.routes);
        if (status) {
          res.status(status);
        }
        res.send('<!doctype html>\n' +
          ReactDOM.renderToString(<Html config={{
            ...config.app, release: config.release, jsFile, cssFile
          }} component={component} store={store}/>)
        );
      }).catch((err) => {
        console.error('DATA FETCHING ERROR:', err.stack);
        res.status(500);
        hydrateOnClient();
      });
    }
  }));
};

/**
 * Render the server not found responses
 * Performs content-negotiation on the Accept HTTP header
 */
exports.renderNotFound = (req, res) => {
  res.status(404).json({
    error: 'Path not found'
  })
};
