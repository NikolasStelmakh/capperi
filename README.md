# README #

### Steps are necessary to get your application up and running ###

1. Install [nodejs](https://nodejs.org/en/)
2. Run command in console ```npm i pm2 -g```
3. Clone repositories
4. Go to root folder in project and run command ```npm i```
5. Run command in console (root folder in project) ```npm start``` - run project
6. npm stop - stop project, pm2 logs - see logs, pm2 list - list of running projects, pm2 kill - kill all apps


### Applications ###

* admin - [http://localhost:3020](http://localhost:3020)
* api - [http://localhost:3010](http://localhost:3010)

* admin-dev [http://nethack-development.northeurope.cloudapp.azure.com:3020/](http://nethack-development.northeurope.cloudapp.azure.com:3020/)