'use strict';

require('./server.babel');

/**
 * Module dependencies.
 */
var app = require('nethack-frontend-core').app;
var server = app.start();
