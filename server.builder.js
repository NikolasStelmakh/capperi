const config = require('nethack-frontend-core').config;
const webpack = require('webpack');
const fs = require('fs');

var bundleStart;

var compiler = webpack(config.webpack);


compiler.plugin('compile', function () {
  console.log('Bundling ...');
  bundleStart = Date.now();
});

compiler.run((err, stats) => {
  if (err) {
    process.exit(1);
  }
});

compiler.plugin('compile', function () {
  console.log('Bundling ...');
  bundleStart = Date.now();
});

// Give notice when it is done compiling, including the time it took.
compiler.plugin('done', () => {
  console.log('Bundled in ' + (Date.now() - bundleStart) + 'ms!');
  process.exit();
});
